﻿//------------------------------------------------------------------------------
// <auto-generated>
//     이 코드는 도구를 사용하여 생성되었습니다.
//     런타임 버전:4.0.30319.42000
//
//     파일 내용을 변경하면 잘못된 동작이 발생할 수 있으며, 코드를 다시 생성하면
//     이러한 변경 내용이 손실됩니다.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Demo_arLog.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "12.0.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute(@"이 프로그램은 공개 프로젝트입니다.
arLog 클래스를 기반으로 동작하는 데모 프로그램 입니다.
제작자 : tindevil@nate.com
===================================================
demo-gitlab : https://gitlab.com/Open_Project/demo_arlog.git
loglib-gitbla : https://gitlab.com/open-class/arLog_CSharp.git
===================================================
사용 중 문의사항은 메일혹은 gitlab 에 보고해주세요
재 배포시에는 출처를 반드시 남겨주시기 바랍니다")]
        public string copyright {
            get {
                return ((string)(this["copyright"]));
            }
        }
    }
}

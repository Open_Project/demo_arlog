﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Demo_arLog
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.Text = string.Format("{0} v{1}",Application.ProductName, Application.ProductVersion);
        }

        delegate void logMessageHandler(DateTime LogTime, string TypeStr, string Message);
        void log_RaiseMsg(DateTime LogTime, string TypeStr, string Message)
        {
            if (this.richTextBox1.InvokeRequired)
            {
                this.richTextBox1.Invoke(new logMessageHandler(log_RaiseMsg), new object[] { LogTime, TypeStr, Message });
            }
            else
            {
                var msg = string.Format("[{0}]\t[{1}]\t{2}\n", LogTime.ToString("HH:mm:ss"), TypeStr, Message);
                this.richTextBox1.AppendText(msg);
                this.richTextBox1.ScrollToCaret();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Pub.init();
            Pub.log.RaiseMsg += log_RaiseMsg;
            Pub.log.Add("프로그램 시작");
            Pub.log.Flush();
            this.Show();
            Application.DoEvents();

            var fSplash = new AboutBox1();
            fSplash.ShowDialog();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Pub.log.Add("프로그램 종료");
            Pub.log.Flush(); //버퍼의 모든 내용을 저장
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //normal
            Pub.log.Add("일반 메세지");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //info
            Pub.log.AddI("정보 메세지");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //attention
            Pub.log.AddAT("경고 메세지");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //error
            Pub.log.AddE("오류 메세지");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //custom
            Pub.log.Add("TEST", "메세지");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            var prc = new System.Diagnostics.Process();
            var si = new System.Diagnostics.ProcessStartInfo(AppDomain.CurrentDomain.BaseDirectory + "log");
            prc.StartInfo = si;
            prc.Start();
        }
    }
}
